<?php namespace codestackers\Subscribers\Controllers;

use Backend\Classes\Controller;
use BackendMenu;

class Campaigns extends Controller
{
    public $implement = [        'Backend\Behaviors\ListController',        'Backend\Behaviors\FormController'    ];

    public $listConfig = 'config_list.yaml';
    public $formConfig = 'config_form.yaml';

    public $requiredPermissions = [
        'admin_campaigns'
    ];

    public function __construct()
    {
        parent::__construct();
        BackendMenu::setContext('Codestackers.Subscribers', 'main-menu-item', 'side-menu-campaigns');
    }
}
