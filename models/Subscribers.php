<?php namespace Codestackers\Subscribers\Models;

use Model;
use System\Models\File;
use Codestackers\Subscribers\Models\Campaigns;
/**
 * Model
 */
class Subscribers extends Model
{
    use \October\Rain\Database\Traits\Validation;

    use \October\Rain\Database\Traits\SoftDelete;

    protected $dates = ['deleted_at'];

    protected $guarded = ['id', 'created_at'];

    protected $fillable = ['campaign_id', 'language_code','company_name','fullname','firstname','lastname','email','gender','zipcode','address','city','subject','message','browser','ip','utm','newsletter','privacy','form_data'];


    /**
     * @var array Validation rules
     */
    public $rules = [
    ];

    /**
     * @var string The database table used by the model.
     */
    public $table = 'codestackers_subscribers_subscribers';

    /**
     * @var array File relations.
     */
    public $attachOne = [
        'image_uploaded'    => ['System\Models\File', 'delete' => 'true' ],
    ];

    /**
     * @var array Database relations.
     */
    public $belongsTo = [
        'campaign' => 'Codestackers\Subscribers\Models\Campaigns',
    ];

    // public function getCampaignIdOptions()
    // {
    //     return Campaigns::OrderBy('name')->list('id','name');
    // }
}
