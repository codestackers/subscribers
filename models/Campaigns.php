<?php namespace Codestackers\Subscribers\Models;

use Model;
use Codestackers\Subscribers\Models\Form;
use Codestackers\Subscribers\Classes\ActiveCampaign;
use October\Rain\Exception\ApplicationException;

/**
 * Model
 */
class Campaigns extends Model
{
    use \October\Rain\Database\Traits\Validation;
    use \October\Rain\Database\Traits\SoftDelete;

    public $implement = ['@RainLab.Translate.Behaviors.TranslatableModel'];

    protected $dates = ['deleted_at'];

    /**
     * @var array Validation rules
     */
    public $rules = [
    ];
    public $jsonable = [
        'data','form'
    ];

    public $translatable = [
        'form'
    ];

    public $belongsTo = [
//        'formfield' => ['Codestackers\Subscribers\Models\Form', 'key' => 'shortcode', 'otherKey' => 'shortcode' ]
    ];
    
    /**
     * @var string The database table used by the model.
     */
    public $table = 'codestackers_subscribers_campaigns';

    public static function getCampaignById($id=-1, $component)
    {
        $data = self::find($id);
        if(! $data)
            throw new ApplicationException("Campaign not found for id: $id");

        $result['data']     = $data->toArray();
        $rules=[];

        if($result['data']['form']) {
            foreach( $result['data']['form'] as $field )
            {
                $field['data']          = $data->toArray();
                $field['form_data']     = Form::where('shortcode', $field['shortcode'] )->first()->toArray();
                $field['hide_labels']   = $component->property('hide_labels');
                $field['form_partial']  = $component->renderPartial('Form::'.$field['shortcode'] .'.htm', $field);
                $result['fields'][]     = $field;
                if(!empty($field['validation_rules']))
                {
                    $rules[$field['name']]  = $field['validation_rules'];
                }
            }    
        }
        $result['rules']    = json_encode($rules);
        return $result;
    }

    public function getShortcodeOptions()
    {
        return Form::pluck('name','shortcode');
    }

    public function getAcListIdOptions() {
        $ac = new ActiveCampaign;
        return $ac->getLists();
    }

    public function getAcFieldNameOptions() {
                $ac = new ActiveCampaign;
                return $ac->getFields();
    }
    
    public function getTypeOptions()
    {
        return [
            "text" => "text", "password" => "password","email" => "email",
            "button" => "button", "checkbox" => "checkbox", "color" => "color", "date" => "date", "datetime-local" => "datetime-local",
            "file" => "file", "hidden" => "hidden", "image" => "image", "month" => "month", "number" => "number",
            "radio" => "radio", "range" => "range", "reset" => "reset", "search" => "search",
            "submit" => "submit", "tel" => "tel",  "time" => "time", "url" => "url", "week" => "week"
        ];
    }
}
