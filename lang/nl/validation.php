<?php 
    return
    [
        'email'                 => 'E-mailadres',
        'required'              => ':attribute is verplicht',
        'firstname'             => 'Voornaam',
        'firstname.required'    => 'Voornaam is verplicht',
        'lastname'              => 'Achternaam',
        'lastname.required'     => 'Achternaam is verplicht',
        'message.required'      => 'U bent vergeten een bericht in te vullen',
        'email.required'        => 'U bent vergeten uw e-mailadres in te vullen',
        'email.email'           => 'Je bent vergeten je email in te vullen',
        'fullname.required'     => 'U bent vergeten uw voornaam in te vullen',
        'terms.required'        => 'U bent vergeten de algemene voorwaarden te accepteren'
    ];
?>
