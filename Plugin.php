<?php namespace Codestackers\Subscribers;

use System\Classes\PluginBase;
use Codestackers\Subscribers\Classes\ActiveCampaign;
use Codestackers\Subscribers\Models\Campaigns;

class Plugin extends PluginBase
{
    public function pluginDetails()
    {
        return [
            'name'        => 'Codestackers Subscribers',
            'description' => 'Subscribe email addresses and passes them on to Active Campaign or MailChimp.',
            'author'      => 'Codestackers',
            'icon'        => 'icon-envelope-square',
            'homepage'    => 'https://codestackers.io/'
        ];
    }

    public function boot()
    {
        \RainLab\Pages\Classes\Page::extend(function($model) {
            $model->addDynamicMethod('getPageOptions', function() {
                $theme = \Cms\Classes\Theme::getEditTheme();
                $pageList = new \RainLab\Pages\Classes\PageList($theme);
                $pages = [];
                foreach ($pageList->listPages() as $name => $pageObject) {
                    $pages[$pageObject->url] = $pageObject->title . ' (' . $pageObject->url . ')';
                }
                return $pages;
            });
            $model->addDynamicMethod('getListOptions', function() {
                $ac = new ActiveCampaign;
                return $ac->getLists();
            });
            $model->addDynamicMethod('getAcFieldNameOptions', function() {
                $ac = new ActiveCampaign;
                return $ac->getFields();
            });            
            $model->addDynamicMethod('getFormIdOptions', function() {
                $options = Campaigns::pluck('name','id');
                return $options;
            });
            $model->addDynamicMethod('getCampaignIdOptions', function() {
                $options = Campaigns::pluck('name','id');
                return $options;
            });                                    

        });        
    }
    public function registerComponents()
    {
        return
        [
            'Codestackers\Subscribers\Components\Contact'       => 'Contact',
            'Codestackers\Subscribers\Components\Newsletter'    => 'Newsletter',
            'Codestackers\Subscribers\Components\Form'          => 'Form',
        ];
    }
    public function registerMailTemplates()
    {
        return [
            'codestackers.subscribers::mail.subscribed',
        ];
    }

    public function registerPageSnippets()
    {
        return $this->registerComponents();
    }

    public function registerSettings()
    {
        return [
            'config' => [
                'label' => 'codestackers.subscribers::lang.plugin.name',
                'category' => 'system::lang.system.categories.cms',
                'icon' => 'oc-icon-user',
                'description' => 'codestackers.subscribers::lang.plugin.description',
                'class' => 'Codestackers\Subscribers\Models\Settings',
                'order' => 800,
            ]
        ];
    }
    public function registerReportWidgets()
    {
        return [
            'Codestackers\Subscribers\ReportWidgets\SubscriberStats' => [
                'label'   => 'Subscribers stats',
                'context' => 'dashboard'
            ]
        ];
    }


}
