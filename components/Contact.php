<?php

namespace Codestackers\Subscribers\Components;

use Cms\Classes\ComponentBase;

use Session;
use Validator;
use Input;
use Request;
use AjaxException;
use ValidationException;
use Flash;
use Lang;

use Codestackers\Subscribers\Classes\ActiveCampaign;
use Codestackers\Subscribers\Models\Settings;
use System\Models\File;

use Codestackers\Subscribers\Models\Subscribers;
use Codestackers\Subscribers\Models\Preview;

class Contact extends ComponentBase
{
    public function componentDetails()
    {
        return [
            'name'        => 'Contact with Upload',
            'description' => 'Upload to subscribers '
        ];
    }

    public function defineProperties()
    {
        return [];
    }

    public function onPreviewImage()
    {
        $file             = (new File())->fromPost(Input::file('uploadImage'));
        $preview        = new Preview;
        if (!$file) return;

        $preview->deleteFilesForSessionID();

        $preview                    = new Preview;
        $preview->session_id        = Session::get('_token');
        $preview->image_uploaded    = Input::file('uploadImage');
        $preview->save();

        // $preview = $this->createPreview();
        // Session::put("order.previewfile", $preview->getPath() );
        return [
            '#preview'         => '<img src="' . $preview->image_uploaded->getThumb(400, 400) . '" id="preview_image" class="img-fluid">',
            //			'#receipt' 	=> $this->renderPartial('receipt')
        ];
    }

    protected function process_active_campaign($data)
    {
        $active_campaign = new ActiveCampaign(
            Settings::instance()->ac_url,
            Settings::instance()->ac_api_key
        );

        setlocale(LC_ALL, 'nl_NL');
        //        $newsletter = isset($data->newsletter) 	? $data->newsletter : null;
        $ac     = [
            //			'first_name'								=> $data['firstname'],
            'last_name'                                    => $data->fullname,
            'email'                                        => $data->email,
            "p[" . $data->campaign->ac_list_id . "]"        => $data->campaign->ac_list_id,
            "tags"                                      => $data->campaign->ac_tag,
        ];

        if (isset($data->newsletter))       $ac["p[1]"]                 = "1";
        if (isset($data->newsletter))       $ac['field[%NEWSLETTER%]']  = "1";
        if (isset($data->privacy))          $ac['field[%PRIVACY%]']     = "1";
        if (isset($data->gender))           $ac['field[%GENDER%]']      = $data->gender;
        if (isset($data->language_code))    $ac['field[%LANGUAGECODE%]'] = $data->language_code;
        if (isset($data->message))          $ac['field[%MESSAGE%]']     = $data->message;
        if (isset($data->subject))          $ac['field[%SUBJECT%]']     = $data->subject;
        $active_campaign->call('contact/sync', $ac);
    }

    public function onSubmit()
    {
        $data                       = post();
        $rules = [
            'name'              => 'required',
            'email'             => 'required|email'
        ];
        $validation = Validator::make($data, $rules,  Lang::get('codestackers.subscribers::validation'));
        if ($validation->fails()) {
            throw new ValidationException($validation);
        };

        $subscriber                 = new Subscribers();
        $data['form_data']          = json_encode($data);
        $subscriber->fill($data);
        $subscriber->save();

        $this->process_active_campaign($subscriber);

        //            '#'.$this->form_id	=> $this->renderPartial('subscribeoct::thankyou',[
        return [
            '#' . post('form_id')    => '<h1 class="form-thank-you" >Bedankt</h1><span>Wij nemen snel contact met je op.</span>',
            'success' => true
        ];
    }

    public function onGetCampaigns()
    {
        return ['1' => 1];
    }

    public function onRun()
    {
        // $this->page['yoga']  = true;
    }
}
