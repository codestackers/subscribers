<?php namespace codestackers\Subscribers\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateCodestackersSubscribersCampaigns extends Migration
{
    public function up()
    {
        Schema::create('codestackers_subscribers_campaigns', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->string('name', 100)->nullable();
            $table->text('description')->nullable();
            $table->string('utm', 150)->nullable();
            $table->string('ac_tag', 100)->nullable();
            $table->integer('ac_list_id')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->index('name', 'idx_name');
            $table->index('ac_list_id', 'idx_ac_list_id');
            $table->index('created_at', 'idx_created_at');
        });

        Schema::create('codestackers_subscribers_subscribers', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id');
            $table->integer('campaign_id')->nullable();
            $table->string('language_code', 2)->nullable();
            $table->string('fullname', 150)->nullable();
            $table->string('firstname', 100)->nullable();
            $table->string('lastname', 100)->nullable();
            $table->string('email', 255)->nullable();
            $table->string('gender', 1)->nullable();
            $table->string('zipcode',150)->nullable();
            $table->string('address',150)->nullable();
            $table->string('city',100)->nullable();
            $table->boolean('winner')->nullable();
            $table->string('subject', 255)->nullable();
            $table->text('message')->nullable();
            $table->text('browser')->nullable();
            $table->string('ip',255)->nullable();
            $table->string('utm',255)->nullable();
            $table->boolean('newsletter')->nullable();
            $table->boolean('privacy')->nullable();
            $table->timestamp('synced_with_ac_at')->nullable();
            $table->timestamp('created_at')->nullable();
            $table->timestamp('updated_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->index('campaign_id','idx_campaign_id');
            $table->index('language_code','idx_language_code');
            $table->index('email','idx_email');
            $table->index('created_at','idx_created_at');
        });

    }

    public function down()
    {
        Schema::dropIfExists('codestackers_subscribers_campaigns');
        Schema::dropIfExists('codestackers_subscribers_subscribers');
    }
}
