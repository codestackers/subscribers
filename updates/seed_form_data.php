<?php namespace Codestackers\Subscribers\Updates;

use Db;
use Seeder;

class SeedFormData extends Seeder
{
    public function run()
    {
        $user = Db::table('codestackers_subscribers_form')->insert([[
            'shortcode'    => 'input',
            'name'         => 'Input field Bootstrap',
            'file'         => 'Form::input'
       ],[
            'shortcode'    => 'checkbox',
            'name'         => 'Checkbox Bootstrap',
            'file'         => 'Form::checkbox'            
        ],[
            'shortcode'    => 'dropdown',
            'name'         => 'Dropdown Bootstrap',
            'file'         => 'Form::checkbox'
        ],[
            'shortcode'    => 'textarea',
            'name'         => 'Text Area Bootstrap',
            'file'         => 'Form::textarea'
        ],[
            'shortcode'    => 'radio',
            'name'         => 'Radio Bootstrap',
            'file'         => 'Form::radio'
        ],[
            'shortcode'    => 'submit',
            'name'         => 'Submit button Bootstrap',
            'file'         => 'Form::checkbox'
        ],[
            'shortcode'    => 'select2',
            'name'         => 'Select 2 dropdown',
            'file'         => 'Form::select2'
        ]]
        );
    }
}