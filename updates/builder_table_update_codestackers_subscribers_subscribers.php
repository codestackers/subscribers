<?php namespace Codestackers\Subscribers\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCodestackersSubscribersSubscribers extends Migration
{
    public function up()
    {
        Schema::table('codestackers_subscribers_subscribers', function($table)
        {
            $table->string('company_name', 150)->after('language_code')->nullable();
            $table->text('form_data')->after('privacy')->nullable();
        });
    }

    public function down()
    {
        Schema::table('codestackers_subscribers_subscribers', function($table)
        {
            $table->dropColumn('company_name');
            $table->dropColumn('form_data');
        });
    }
}
