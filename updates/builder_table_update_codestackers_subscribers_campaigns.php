<?php namespace Codestackers\Subscribers\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCodestackersSubscribersCampaigns extends Migration
{
    public function up()
    {
        Schema::table('codestackers_subscribers_campaigns', function($table)
        {
            $table->boolean('send_data_to_ac')->nullable();
            $table->boolean('send_data_to_url')->nullable();
            $table->boolean('send_data_to_mail')->nullable();
            $table->text('send_data_url')->nullable();
            $table->text('send_data_mail_subject')->nullable();
            $table->text('send_data_mail_from')->nullable();
            $table->text('send_data_mail_to')->nullable();
            $table->text('form')->nullable();
        });

        Schema::dropIfExists('codestackers_subscribers_form');
        Schema::create('codestackers_subscribers_form', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->string('shortcode', 15);
            $table->string('name', 50)->nullable();
            $table->text('html')->nullable();
            $table->string('file', 150);
        });
    }

    public function down()
    {
        Schema::table('codestackers_subscribers_campaigns', function($table)
        {
            $table->dropColumn('data');
            $table->dropColumn('send_data_to_ac');
            $table->dropColumn('send_data_to_url');
            $table->dropColumn('send_data_to_mail');
            $table->dropColumn('send_data_url');
            $table->dropColumn('send_data_mail_subject');
            $table->dropColumn('send_data_mail_from');
            $table->dropColumn('send_data_mail_to');
            $table->dropColumn('form');
        });
        Schema::dropIfExists('codestackers_subscribers_form');
        
    }
}