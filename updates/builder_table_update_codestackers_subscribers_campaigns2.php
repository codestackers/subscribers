<?php namespace Codestackers\Subscribers\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCodestackersSubscribersCampaigns2 extends Migration
{
    public function up()
    {
        Schema::table('codestackers_subscribers_campaigns', function($table)
        {
            $table->string('comments_icon', 150)->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('codestackers_subscribers_campaigns', function($table)
        {
            $table->dropColumn('comments_icon');
        });
    }
}