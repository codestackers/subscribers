<?php namespace Codestackers\Subscribers\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableUpdateCodestackersSubscribersCampaigns2 extends Migration
{
    public function up()
    {
        Schema::table('codestackers_subscribers_campaigns', function($table)
        {
            $table->string('send_data_zapier_url', 200)->nullable();
            $table->boolean('send_data_to_zapier')->nullable();
        });
    }
    
    public function down()
    {
        Schema::table('codestackers_subscribers_campaigns', function($table)
        {
            $table->dropColumn('send_data_zapier_url');
            $table->dropColumn('send_data_to_zapier');
        });
    }
}
